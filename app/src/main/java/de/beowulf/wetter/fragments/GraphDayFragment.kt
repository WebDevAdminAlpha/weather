package de.beowulf.wetter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.beowulf.wetter.GlobalFunctions
import de.beowulf.wetter.R
import de.beowulf.wetter.adapter.DataPoint
import de.beowulf.wetter.databinding.FragmentGraphBinding
import java.text.SimpleDateFormat
import java.util.*

class GraphDayFragment : Fragment() {

    private lateinit var binding: FragmentGraphBinding

    private val day = arrayOfNulls<String>(8)
    private val temp = arrayOfNulls<Int>(8)
    private val precipitation = arrayOfNulls<Int>(8)
    private val gf = GlobalFunctions()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGraphBinding.inflate(layoutInflater)
        val view: View = binding.root

        gf.initializeContext(context!!)

        val tempTitle: String = getString(R.string.temperature) + gf.unitTemp()
        binding.TempTitle.text = tempTitle

        binding.graphView2.setData(precipitation(), null,true)
        binding.graphView.setData(temp("min"), temp("max"), false)

        return view
    }

    private fun temp(type: String): List<DataPoint> {
        val jsonObj = gf.result()

        for (i: Int in 0..7) {
            temp[i] = (gf.graphTemp(jsonObj.getJSONArray("daily").getJSONObject(i).getJSONObject("temp").getDouble(type)))
        }
        return (0..7).map {
            DataPoint(it, temp[it]!!, day[it])
        }
    }

    private fun precipitation(): List<DataPoint> {
        val jsonObj = gf.result()

        for (i: Int in 0..3) {
            day[i * 2] = SimpleDateFormat(getString(R.string.daydate), Locale.getDefault()).format(
                Date(
                    jsonObj.getJSONArray("daily").getJSONObject(i * 2).getLong("dt") * 1000
                )
            )
        }
        for (i: Int in 0..7) {
            precipitation[i] =
                (jsonObj.getJSONArray("daily").getJSONObject(i).getDouble("pop") * 100).toInt()
        }
        return (0..7).map {
            DataPoint(it, precipitation[it]!!, day[it])
        }
    }
}