package de.beowulf.wetter.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RemoteViews
import de.beowulf.wetter.GlobalFunctions
import de.beowulf.wetter.R
import de.beowulf.wetter.StartActivity
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class AppWidget : WidgetProvider() {

    private val gf = GlobalFunctions()

    override fun onAppWidgetOptionsChanged(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        newOptions: Bundle
    ) {
        gf.initializeContext(context)
        val views = RemoteViews(context.packageName, R.layout.app_widget)

        resizeWidget(newOptions, views)
        //Start app, when you click on the widget
        val configIntent = Intent(context, StartActivity::class.java)
        val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
        views.setOnClickPendingIntent(R.id.AppWidget, configPendingIntent)

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views)
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        val gf = GlobalFunctions()
        gf.initializeContext(context)
        val views = RemoteViews(context.packageName, R.layout.app_widget)

        if (gf.getInitialized()) {
            val jsonObj = gf.result()

            //get data
            val current: JSONObject = jsonObj.getJSONObject("current")
            val currentWeather: JSONObject = current.getJSONArray("weather").getJSONObject(0)

            val image: Int = gf.icon(currentWeather.getString("icon"))
            val actualTemp: String = gf.convertTemp(current.getDouble("temp"))
            val statusText: String = currentWeather.getString("description")
            val wind: String =
                gf.convertSpeed(current.getDouble("wind_speed")) + " (${
                    gf.degToCompass(current.getInt("wind_deg"))
                })"

            var rainSnow: String
            var type = "snow"
            when {
                current.has("rain") -> {
                    rainSnow = gf.convertRain(current.getJSONObject("rain").getDouble("1h"))
                    type = "rain"
                }
                current.has("snow") -> {
                    rainSnow = gf.convertRain(current.getJSONObject("snow").getDouble("1h"))
                }
                else -> {
                    rainSnow = gf.convertRain(0.0)
                    type = "rain"
                }
            }
            if (type == "rain") {
                views.setViewVisibility(R.id.Rain, View.VISIBLE)
                views.setViewVisibility(R.id.Snow, View.GONE)
            } else {
                views.setViewVisibility(R.id.Rain, View.GONE)
                views.setViewVisibility(R.id.Snow, View.VISIBLE)
            }
            val precipitation: Double =
                jsonObj.getJSONArray("hourly").getJSONObject(0).getDouble("pop") * 100
            rainSnow += " (${precipitation.toString().split(".")[0]}%)"
            val sunrise: String =
                SimpleDateFormat(gf.getTime(), Locale.ROOT).format(
                    Date(
                        current.getLong("sunrise") * 1000
                    )
                )
            val sunset: String =
                SimpleDateFormat(gf.getTime(), Locale.ROOT).format(
                    Date(
                        current.getLong("sunset") * 1000
                    )
                )
            val pressure: String = current.getString("pressure") + "hPa"
            val humidity: String = current.getString("humidity") + "%"

            for (appWidgetId in appWidgetIds) {

                //set View
                views.setImageViewResource(R.id.Status_Image, image)
                views.setTextViewText(R.id.actualTemp, actualTemp)
                views.setTextViewText(R.id.Status_Text, statusText)
                views.setTextViewText(R.id.Wind, wind)
                views.setTextViewText(R.id.RainSnow, rainSnow)
                views.setTextViewText(R.id.Sunrise, sunrise)
                views.setTextViewText(R.id.Sunset, sunset)
                views.setTextViewText(R.id.Pressure, pressure)
                views.setTextViewText(R.id.Humidity, humidity)

                val appWidgetOptions = appWidgetManager.getAppWidgetOptions(appWidgetId)
                resizeWidget(appWidgetOptions, views)

                //Start app, when you click on the widget
                val configIntent = Intent(context, StartActivity::class.java)
                val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
                views.setOnClickPendingIntent(R.id.AppWidget, configPendingIntent)

                // Instruct the widget manager to update the widget
                appWidgetManager.updateAppWidget(appWidgetId, views)
            }
        }
    }
}

private fun resizeWidget(appWidgetOptions: Bundle, views: RemoteViews) {
    val minWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
    /*val maxWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)
    val minHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)*/
    val maxHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)

    when {
        minWidth < 90 -> {
            views.setViewVisibility(R.id.actualTemp, View.GONE)
            views.setViewVisibility(R.id.Status_Text, View.GONE)
            views.setViewVisibility(R.id.WindRain, View.GONE)
        }
        minWidth < 245 -> {
            views.setViewVisibility(R.id.actualTemp, View.VISIBLE)
            views.setViewVisibility(R.id.Status_Text, View.VISIBLE)
            views.setViewVisibility(R.id.WindRain, View.GONE)
        }
        else -> {
            views.setViewVisibility(R.id.actualTemp, View.VISIBLE)
            views.setViewVisibility(R.id.Status_Text, View.VISIBLE)
            views.setViewVisibility(R.id.WindRain, View.VISIBLE)
        }
    }
    when {
        maxHeight < 110 || minWidth < 250 -> {
            views.setViewVisibility(R.id.moreInfos, View.GONE)
        }
        else -> {
            views.setViewVisibility(R.id.moreInfos, View.VISIBLE)

        }
    }
}
