package de.beowulf.wetter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import de.beowulf.wetter.adapter.PagerAdapter

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(GlobalFunctions().getTheme(this))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager = findViewById<ViewPager>(R.id.pager)
        val adapter = PagerAdapter(
            supportFragmentManager
        )
        viewPager.adapter = adapter

        viewPager.currentItem = 2
    }

    override fun onBackPressed() {
        val viewPager = findViewById<ViewPager>(R.id.pager)
        if (viewPager.currentItem != 2) {
            if (viewPager.currentItem < 2)
                viewPager.currentItem ++
            else
                viewPager.currentItem --
        } else {
            super.onBackPressed()
        }
    }
}